(ns ctp.dijkstra
  (:require [ctp.data :as data]))

(defn shortest-path
  "computes the shortest path in the graph between s and t;
  returns the path as a sequence of nodes and the length
  of the path."
  [graph s t]
  (loop [dist (assoc
                (vec (repeat (count graph) (/ 1. 0.)))
                s 0.)
         prev (vec (repeat (count graph) nil))
         Q (set (range (count graph)))]
    (when (seq Q)
      (let [u (reduce (fn [u v]
                        (if (< (dist u) (dist v))
                          u v))
                      (first Q) Q)]
        (if (= u t)
          (loop [u t
                 path (list)]
            (if (prev u)
              (recur (prev u) (cons u path))
              [path (dist t)]))
          (let [[dist prev]
                (loop [neighbors (graph u)
                       dist dist
                       prev prev]
                  (if (seq neighbors)
                    (let [[[v d] & neighbors] neighbors
                          alt (+ (dist u) d)]
                      (if (< alt (dist v))
                        (recur neighbors
                               (assoc dist v alt)
                               (assoc prev v u))
                        (recur neighbors dist prev)))
                    [dist prev]))]
            (recur dist prev (disj Q u))))))))