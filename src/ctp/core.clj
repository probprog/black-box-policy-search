(ns ctp.core
  (:require [clojure.tools.cli :as cli]
            [clojure.data.json :as json]
            [anglican
             [state :refer [initial-state get-predicts]]
             [bbvb :refer [get-variational]]
             [inference :refer [print-predicts]]
             [core :refer [doquery]]])
  (:use [anglican runtime emit])
  (:use [ctp model data]))

(extend anglican.runtime.gamma-distribution 
  json/JSONWriter 
  {:-write (fn [obj out] 
             (json/write (select-keys obj [:shape :rate]) out))})

(def +instance-id+ :20a)
(def +policy-types+ [:node-policy :edge-policy :eager-edge-policy])
(def +baseline-types+ [:clairvoyant-agent :random-agent :optimistic-agent])
(def +open-probability+ 1.0)

(def +number-of-particles+ 1000)
(def +number-of-steps-range+ [1 2 5 10 20 50 100 200 500 1000])
(def +number-of-test-episodes+ 1000)

(def +base-stepsize+ 0.1)
(def +adagrad+ 0.9)
(def +robbins-monro+ 0.5)

(defn test-policy
  ([instance open-probability simulate-agent make-policy number-of-episodes proposals]
  (let [predicts (->> (doquery :bbeb
                                ctp
                                [instance
                                 open-probability
                                 simulate-agent
                                 make-policy]
                                :only [:policy]
                                :number-of-particles number-of-episodes
                                :base-stepsize 0.0
                                :adagrad false
                                :initial-proposals proposals
                                :stripdown false
                                :warmup false)
                       (filter #(> (:log-weight %) (/ -1.0 0.0)))
                       (take number-of-episodes)
                       (map get-predicts))
         distances (map :distance predicts)
         counts (reduce (partial merge-with +)
                        (map :counts predicts))]
     [distances counts]))
  ([instance open-probability simulate-agent make-policy number-of-episodes]
   (instance open-probability simulate-agent make-policy number-of-episodes nil)))
                      
(defn trial
  [policy-type instance open-probability  
   number-of-train-steps number-of-test-episodes
   & {:keys [number-of-particles
             base-stepsize 
             adagrad 
             robbins-monro] 
      :or {number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}
      :as options}]
  (let [make-policy (var-get (resolve (symbol "ctp.model" (name policy-type))))
        state (->> (doquery :bbeb ctp
                            [instance open-probability dfs-agent make-policy]
                            :only [:policy]
                            :number-of-particles number-of-particles
                            :base-stepsize base-stepsize
                            :adagrad adagrad
                            :robbins-monro robbins-monro
                            :stripdown false
                            :warmup false)
                   (drop (* number-of-train-steps number-of-particles))
                   first)
        proposals (get-variational state)
        [distances counts] (test-policy instance
                                       
 open-probability
                                        dfs-agent
                                        make-policy
                                        number-of-test-episodes
                                        proposals)]
    [distances counts proposals]))

(defn baseline
  [agent-type instance open-probability num-episodes]
  (if (= agent-type :optimistic-agent)
    (test-policy instance
                 open-probability
                 dfs-agent
                 optimistic-policy
                 num-episodes
                 nil)
    (let [simulate-agent (var-get (resolve (symbol "ctp.model" (name agent-type))))]
      (test-policy instance
                   open-probability
                   simulate-agent
                   nil
                   num-episodes
                   nil))))

(defn run-trials
  [& {:keys [policy-types
             instance-id
             open-probability
             number-of-steps-range
             number-of-test-episodes
             number-of-particles
             base-stepsize 
             adagrad 
             robbins-monro] 
      :or {policy-types +policy-types+
           instance-id +instance-id+
           open-probability +open-probability+
           number-of-steps-range +number-of-steps-range+
           number-of-test-episodes +number-of-test-episodes+
           number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}
      :as options}]
  (doseq [policy-type policy-types
          number-of-train-steps number-of-steps-range]
    (let [[distances counts proposals] (trial policy-type
                                              (instances instance-id)
                                              open-probability
                                              number-of-train-steps
                                              number-of-test-episodes
                                              :number-of-particles number-of-particles
                                              :base-stepsize base-stepsize
                                              :adagrad adagrad
                                              :robbins-monro robbins-monro)
               edges (into {}
                           (map (fn [e]
                                  [e (sort e)])
                                (keys counts)))
               file-name (format "results/ctp/ctp-%s-%s-p%.1f-rho%.1f-gamma%.1f-kappa%.1f-n%04d-steps%05d-test%d.json"
                                 (name policy-type)
                                 (name instance-id)
                                 open-probability
                                 base-stepsize
                                 adagrad
                                 robbins-monro
                                 number-of-particles
                                 number-of-train-steps
                                 number-of-test-episodes)]
           (println (format "writing: %s" file-name))
           (->> options
                (merge {:distances distances
                        :counts counts
                        :edges edges
                        :proposals proposals})
                (json/write-str)
                (spit file-name)))))

(defn run-baselines
  [& {:keys [baseline-types
             instance-id
             open-probability
             number-of-test-episodes]
      :or {baseline-types +baseline-types+
           instance-id +instance-id+
           open-probability +open-probability+
           number-of-test-episodes +number-of-test-episodes+}
      :as options}]
  (doseq [baseline-type baseline-types]
    (let [[distances counts] (baseline baseline-type
                                       (instances instance-id)
                                       open-probability
                                       number-of-test-episodes)
               edges (into {}
                           (map (fn [e]
                                  [e (sort e)])
                                (keys counts)))
               file-name (format "results/ctp/ctp-%s-%s-p%.1f-test%d.json"
                                 (name baseline-type)
                                 (name instance-id)
                                 open-probability
                                 number-of-test-episodes)]
           (println (format "writing: %s" file-name))
           (->> options
                (merge {:distances distances
                        :counts counts
                        :edges edges})
                (json/write-str)
                (spit file-name)))))

(def cli-options
  [["-i" 
    "--instance-id NAME" 
    "Instance name"
    :parse-fn keyword
    :default +instance-id+
    :validate [#(instances %) "instance does not exist"]]

   ["-o" 
    "--open-probability P" 
    "Probability of edge being open"
    :parse-fn #(Double/parseDouble %)
    :default +open-probability+
    :validate [#(<= 0. % 1.) "must be between 0 and 1"]]

   ["-r" 
    "--number-of-steps-range RANGE" 
    "Range of numbers of steps"
    :parse-fn #(read-string (str "[" % "]"))
    :default +number-of-steps-range+]

   ["-p" 
    "--policy-types TYPES" 
    "Policy types to train and test"
    :parse-fn #(read-string (str "[" % "]"))
    :default +policy-types+]

   ["-b" 
    "--baseline-types TYPES" 
    "Baseline types to train and test"
    :parse-fn #(read-string (str "[" % "]"))
    :default +baseline-types+]
   
   ["-n" 
    "--number-of-particles N" 
    "Number of particles to use to compute each gradient step"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-particles+]

   ["-m" 
    "--number-of-test-episodes M" 
    "Number of episodes to run to evaluate the learned policy hypereparameters"
    :parse-fn #(Integer/parseInt %)
    :default +number-of-test-episodes+]

   ["-s" 
    "--base-stepsize RHO" 
    "Base step size"
    :parse-fn #(Double/parseDouble %)
    :default +base-stepsize+]

   ["-a" 
    "--adagrad GAMMA" 
    "Decay rate for AdaGrad/RMSProp"
    :parse-fn #(Double/parseDouble %)
    :default +adagrad+]

   ["-k" 
    "--robbins-monro KAPPA" 
    "Decay rate for step size"
    :parse-fn #(Double/parseDouble %)
    :default +robbins-monro+]

   ["-h" 
    "--help" 
    "Print usage summary and exit"]])

(defn -main
  [& args]
  (let [{:keys [options arguments errors summary]
         :as parsed-options}
        (cli/parse-opts args cli-options)
        summary (format "baselines|trials OPTIONS\n%s" summary)]
    (cond
     (:help options) (println summary)
     errors (println errors)
     (empty? arguments) (println summary)

     :else
     (do
       (println options)
       (cond 
         (= (first arguments) "trials")
         (apply run-trials (apply concat options))
         (= (first arguments) "baselines")
         (apply run-baselines (apply concat options)))))))

